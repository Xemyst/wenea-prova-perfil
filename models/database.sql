CREATE SCHEMA IF NOT EXISTS wenea;

CREATE TABLE IF NOT EXISTS wenea.chargepoin (
  id SERIAL PRIMARY KEY,
  name TEXT UNIQUE NOT NULL,
  id_status INT NOT NULL REFERENCES wenea.state(id),
  created_at TIMESTAMP DEFAULT NOW(),
  deleted_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS wenea.state (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT wenea.state (value)
VALUES
(ready),
(charging),
(waiting),
(error);
