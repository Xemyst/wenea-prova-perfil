const db = require('./db')


const chargepoint = {
  getAllChargepoints: async () => {
    return db.query(`
      SELECT
        c.id,
        c.name,
        s.value as status
      FROM
        wenea.chargepoint as c
      LEFT JOIN
        wenea.state as s
        ON c.id_status = s.id
      WHERE
        c.deleted_at IS NULL;
  `)},
  getChargepointById: async ({id}) => {
    return db.query(`
      SELECT
        c.id,
        c.name,
        s.value as status
      FROM
        wenea.chargepoint as c
      LEFT JOIN
        wenea.state as s
        ON c.id_status = s.id
      WHERE
        c.id=$1 AND deleted_at IS NULL;
    `, [id])
  },
  addChargepoint: async ({name,state}) => {
    return db.query(`
      INSERT INTO wenea.chargepoint (name,id_status)
      VALUES ($1,(SELECT id FROM wenea.state where value = $2));
      `,[name, state])
  },
  changeCharpointState: async ({id,state}) => {
    return db.query(`
      UPDATE wenea.chargepoint
      SET
      id_status = (SELECT id
        FROM wenea.state
        WHERE value=$2)
      WHERE id = $1 AND deleted_at IS NULL RETURNING name;
      `,[id, state])
  }, deleteChargepointById: async ({id}) => {
      return db.query(`
        UPDATE wenea.chargepoint
        SET
        deleted_at = Now()
        WHERE id = $1 AND deleted_at IS NULL;
        `,[id])
    }

}


module.exports = chargepoint
