const { Pool } = require('pg')
const pool = new Pool()

pool.query(`
  CREATE SCHEMA IF NOT EXISTS wenea;

  CREATE TABLE IF NOT EXISTS wenea.state (
    id SERIAL PRIMARY KEY,
    value TEXT NOT NULL UNIQUE
  );
  CREATE TABLE IF NOT EXISTS wenea.chargepoint (
    id SERIAL PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    id_status INT NOT NULL REFERENCES wenea.state(id),
    created_at TIMESTAMP DEFAULT NOW(),
    deleted_at TIMESTAMP
  );
  INSERT INTO wenea.state (value)
  VALUES
  ('ready'),
  ('charging'),
  ('waiting'),
  ('error');
`)
.catch(err => {
  console.log('Aqui dona error per repetir insersions de state, pero es per garantir lexistencia en cada execucio, aixo en deployment no seria aixi')
})


module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback)
  },
}
