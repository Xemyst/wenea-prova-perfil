FROM node:10.17.0-alpine3.9

WORKDIR /usr/app

COPY package*.json ./
RUN npm install

COPY . ./


EXPOSE 3000
CMD ["node","server.js"]
