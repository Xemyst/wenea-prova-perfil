const chargepoint = require('express').Router()
const chargepointDb = require('../models/chargepoint.js')
const upload = require('multer')()

chargepoint.get('/',(req, res)=>{
  chargepointDb.getAllChargepoints()
  .then(data => {
    data = data.rows
    res.status(200).send(data)
  })
  .catch(err => {
    res.status(500).send({msg: 'internal error'})
  })
})
chargepoint.post('/',upload.none(),(req, res)=>{
  chargepointDb.addChargepoint(req.body)
  .then(data => {
    res.status(200).send({msg:'done'})
  })
  .catch(err => {
    if(parseInt(err.code) === 23505) {
      res.status(404).send({msg: 'name used'})
    }else {
      res.status(500).send({msg: 'internal error'})
    }
  })
})
chargepoint.get('/:id',(req, res)=> {
  chargepointDb.getChargepointById(req.params)
  .then(data => {
    res.status(200).send(data.rows[0])
  })
  .catch(err => {
    res.status(500).send({msg: 'internal error'})
  })
})

chargepoint.delete('/:id',(req, res)=>{
  chargepointDb.deleteChargepointById(req.params)
  .then(data => {
    if(data.rowCount === 0) {
      res.status(404).send({msg:'this charger dosnt exists'})
    }else {
      res.status(200).send({msg:'done'})
    }
  })
  .catch(err => {
    res.status(500).send({msg: 'internal error'})
  })
})

chargepoint.put('/state',upload.none(),(req, res)=>{


  chargepointDb.changeCharpointState(req.body)
  .then(data => {
    if(data.rows.length === 1){

    const name = data.rows[0].name
    global.io.emit('newState',{name, state: req.body.state})
    res.status(200).send({msg: 'done'})
  }else{
    res.status(404).send({msg: 'this charger dosnt exists'})
  }
  })
  .catch(err => {
    if(parseInt(err.code) === 23502) {
        res.status(404).send({msg: 'that state dosnt exists'})
    }else {
      res.status(500).send({msg: 'internal error'})
    }
  })
})

module.exports = chargepoint
