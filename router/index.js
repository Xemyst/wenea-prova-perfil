const router = require('express').Router()
const chargepoint = require('./chargepoint')
router.use('/chargepoint', chargepoint)

module.exports = router
