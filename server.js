require('dotenv').config()
const express = require('express')
const app = express()
const router = require('./router')
const http = require('http').createServer(app);
global.io = require('socket.io')(http);

const PORT = process.env.PORT || 3000

io.on('connection', (socket) => {
  socket.emit('newState',{id:'state'})
});

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/socket_client.html');
});

app.use(router)


http.listen(PORT, () => {
  console.log(`listening on ${PORT}`);
});
